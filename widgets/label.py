from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, NumericProperty
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from widgets.icons import ICONS


Builder.load_file('widgets/kv/label.kv')


class BHIconLabel(Label):
    icon = StringProperty("")
    font_name = StringProperty("Icon")
    font_size = NumericProperty(40)

    def on_icon(self, *args):
        self.text = ICONS.get(self.icon, "")


class ScrollableLabel(ScrollView):
    text = StringProperty('')
    color = ListProperty([1, 1, 1, 1])
