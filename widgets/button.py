from kivy.properties import OptionProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from kivy.uix.image import Image
from widgets.label import BHIconLabel


class BHButton(Button):
    button_type = OptionProperty('blue', options=['blue', 'white'])
    font_size = NumericProperty(17)

    def __init__(self, **kwargs):
        super(BHButton, self).__init__(**kwargs)
        self.set_default_color()

    def set_white_color(self):
        self.color = (0, 0, 0, 1)
        self.background_normal = 'assets/images/buttons/white_button.png'
        self.background_down = 'assets/images/buttons/white_button_pressed.png'

    def set_default_color(self):
        self.color = (1, 1, 1, 1)
        self.background_normal = 'assets/images/buttons/blue_button.png'
        self.background_down = 'assets/images/buttons/blue_button_pressed.png'

    def on_button_type(self, obj, val):
        if val == 'blue':
            obj.set_default_color()
        else:
            self.set_white_color()


class ImageButton(ButtonBehavior, Image):
    pass


class IconButton(ButtonBehavior, BHIconLabel):

    def _do_press(self):
        self.color = [0.13, 0.59, 0.95, 1]
        super(IconButton, self)._do_press()

    def _do_release(self, *args):
        self.color = [0, 0, 0, 1]
        super(IconButton, self)._do_release()
