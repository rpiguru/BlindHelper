from kivy.factory import Factory

from widgets.button import BHButton
from widgets.separator import Separator

from widgets.label import ScrollableLabel, BHIconLabel

Factory.register('ScrollableLabel', cls=ScrollableLabel)
Factory.register('BHButton', cls=BHButton)
Factory.register("BHIconLabel", cls=BHIconLabel)
Factory.register('Separator', cls=Separator)
