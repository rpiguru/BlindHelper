#!/usr/bin/env bash
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
cd ${cur_dir}/..
git config user.email "rpiguru@techie.com"
git config user.name "RPi Guru"
rm -f .git/index.lock
rm .git/index
git add .
git reset --hard
git pull
