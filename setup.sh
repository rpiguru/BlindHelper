#!/usr/bin/env bash

echo "Installing BlindHelper Service..."
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sed -i -- "s/gitlab.com/gitlab+deploy-token-bh:dR6XZNz8hpqnFaqsEfz5@gitlab.com/g" .git/config

sudo apt update

sudo apt install -y build-essential python3-dev python3-smbus libpython3-dev python3-pip gcc git libasound2-dev
sudo apt install -y libbluetooth-dev libboost-python-dev libboost-thread-dev libglib2.0-dev bluez bluez-hcidump bluealsa pulseaudio-module-bluetooth
sudo pulseaudio -k
sudo pulseaudio --start
sudo adduser lp $(whoami)
sudo sed -i -- "s/ExecStart=\/usr\/lib\/bluetooth\/bluetoothd/ExecStart=\/usr\/lib\/bluetooth\/bluetoothd --noplugin=sap/g" /etc/systemd/system/bluetooth.target.wants/bluetooth.service
sudo sed -i -- "s/ExecStart=\/usr\/lib\/bluetooth\/bluetoothd/ExecStart=\/usr\/lib\/bluetooth\/bluetoothd --noplugin=sap/g" /lib/systemd/system/bluetooth.service
sudo systemctl daemon-reload
sudo service bluetooth restart

sudo pip3 install -U pip setuptools
sudo pip3 install pyserial evdev paho-mqtt pyusb RPi.GPIO pyalsaaudio pybluez pexpect

cd /tmp
git clone https://github.com/adafruit/Adafruit_Python_ADS1x15.git
cd Adafruit_Python_ADS1x15
sudo python3 setup.py install


echo "Configuring the Game HAT from WaveShare"
echo "hdmi_force_hotplug=1" | sudo tee -a /boot/config.txt
echo "hdmi_group=2" | sudo tee -a /boot/config.txt
echo "hdmi_mode=87" | sudo tee -a /boot/config.txt
echo "hdmi_cvt 640 480 60 1 0 0 0" | sudo tee -a /boot/config.txt
echo "hdmi_drive=2" | sudo tee -a /boot/config.txt
echo "max_usb_current=1" | sudo tee -a /boot/config.txt
echo "gpu_mem=512" | sudo tee -a /boot/config.txt
echo "dtparam=i2c_arm=on" | sudo tee -a /boot/config.txt
echo "dtparam=i2c1=on" | sudo tee -a /boot/config.txt
echo "i2c-dev" | sudo tee -a /etc/modules
echo "i2c-bcm2708" | sudo tee -a /etc/modules
echo "snd_bcm2835" | sudo tee -a /etc/modules

echo "Install Kivy now..."
sudo apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev
sudo apt-get install -y libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-bad
sudo apt-get install -y gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-omx
sudo apt-get install -y gstreamer1.0-alsa libmtdev-dev xclip

sudo pip3 install Cython==0.25.2
cd /tmp
git clone https://github.com/kivy/kivy.git
cd kivy
git checkout 678a055
sudo pip3 install .

cd ${cur_dir}/script

# Install custom splash video
sudo bash install_custom_splash_screen.sh

sudo apt install -y espeak
echo "Installing BRLTTY"
#sudo apt install -y brltty
#sudo apt install -y python3-brlapi
#sudo cp ${cur_dir}/conf/brltty.conf /etc/brltty.conf
#sudo apt install gnome-orca

echo "Disabling the booting logo..."
echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt

# Disable the blinking cursor
sudo sed -i -- "s/^exit 0/TERM=linux setterm -foreground black >\/dev\/tty0\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/TERM=linux setterm -clear all >\/dev\/tty0\\nexit 0/g" /etc/rc.local

sudo apt install screen
sudo sed -i -- "s/^exit 0/amixer cset numid=3 2\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -mS bh -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S bh -X stuff \"cd \/home\/pi\/BlindHelper\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S bh -X stuff \"python3 main.py\\\\r\"\\nexit 0/g" /etc/rc.local

echo "Finished Installation, please REBOOT now!"
