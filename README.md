#  Multifunction Raspberry Pi Accessibility Device

## Components

- Raspberry Pi 3 B+
    
    https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/

- 7" touchscreen from Waveshare

    https://www.waveshare.com/product/mini-pc/raspberry-pi/displays/7inch-hdmi-lcd-h.htm

- Mitutoyo Digital Caliper (Mitutoyo CD-6" AX)
    
    https://shop.mitutoyo.eu/web/mitutoyo/en/mitutoyo/01.01.041/USB%20Input%20Tool%20Direct%20%20(Digimatic%20USB)/$catalogue/mitutoyoData/PR/06AFM380A/public/index.xhtml%3bjsessionid=75C02CE802F4BDD3C4CD6E41BA3D215A

- Digital Scale (Ohaus Ranger Count 3000)
    
    https://us.ohaus.com/en-US/Ranger3000Count 

    https://us.ohaus.com/en-US/Products/Accessories/Other/Interface-Cables-and-Kits/USB-Kit-R31-RC31-V71

- Electronic Vacuum Gauge
    
    http://ljengineering.com/digital_detail.html 

- ADS1115 to read an analog voltage from the Vacuum Gauge
    
    https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/ads1015-slash-ads1115

- Metec Flat 20 Braille Display
    
    http://web.metec-ag.de/en/produkte-braille-zeilen.php 


## Installation
    
- Download the latest Raspbian Stretch **Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/) and flash your micro SD card.

- Enable *I2C* interface on RPi by following this guide:
    
    https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c

- Clone this repository
    
    
        cd ~
        sudo apt update
        sudo apt install git
        git clone https://gitlab.com/rpiguru/BlindHelper

- Install everything and reboot!
        
        
        cd BlindHelper
        bash setup.sh
