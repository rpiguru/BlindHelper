import os
import sys
import time
import serial
import serial.tools.list_ports

_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


from settings import SCALE_VID, SCALE_PID, HDCS_SCALE_VID, HDCS_SCALE_PID
from utils.common import logger


class ScaleReader:

    def __init__(self, vid=SCALE_VID, pid=SCALE_PID, name="Scale"):
        self._ser = None
        self.vid = vid
        self.pid = pid
        self.name = name
        self.connect()

    def connect(self):
        port = None
        for dev in serial.tools.list_ports.comports():
            if dev.pid == self.pid and dev.vid == self.vid:
                port = dev.device
                logger.info('Found {} - {}'.format(self.name, port))
        if port is not None:
            try:
                self._ser = serial.Serial(port=port, timeout=1)
                return True
            except Exception as e:
                logger.error('Scale: Failed to open device({}) - {}'.format(port, e))
                self._ser = None

    def read_data(self):
        """
        Read data from the serial line.
        Example data:
                        Count mode:     922     PCS
                       Weight mode:     1.170   oz    N
        :return:
        """
        try:
            if self._ser is None:
                self.connect()
            if self._ser:
                data = self._ser.readline().decode().strip()
                return data
        except Exception as e:
            logger.error('Failed to read data from scale - {}'.format(e))
            self._ser = None

    def close(self):
        if self._ser:
            self._ser.close()
            self._ser = None


class HDCSScaleReader(ScaleReader):

    _ser = None

    def __init__(self, vid=HDCS_SCALE_VID, pid=HDCS_SCALE_PID):
        super().__init__(vid, pid, "HDCS Scale")

    def read_data(self):
        """
        Read data from the serial line.
        :return:
        """
        try:
            if self._ser is None:
                self.connect()
            if self._ser:
                buf = self._ser.readline().decode().strip()
                if buf:
                    # Sample data: `W:+        16oz`
                    data = {
                        'state': 'normal' if buf.startswith('W') else 'overload',
                        'unit': buf[-2:],
                        'value': float(buf[3:-2].strip())
                    }
                    return data
        except Exception as e:
            logger.error('Failed to read data from scale - {}'.format(e))
            self._ser = None

    def close(self):
        if self._ser:
            self._ser.close()
            self._ser = None


if __name__ == '__main__':

    s = HDCSScaleReader()

    while True:
        try:
            print(s.read_data())
            time.sleep(1)
        except KeyboardInterrupt:
            break
    s.close()
