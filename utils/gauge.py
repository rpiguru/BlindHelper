import time
import serial
import serial.tools.list_ports

from settings import GAUGE_VID, GAUGE_PID, ADC_ZERO_VAL, ADC_RATIO, PRESSURE_ZERO_VAL
from utils.common import logger


class GaugeReader:

    _ser = None

    def __init__(self, vid=GAUGE_VID, pid=GAUGE_PID):
        self.vid = vid
        self.pid = pid
        self.connect()

    def connect(self):
        port = None
        for dev in serial.tools.list_ports.comports():
            if dev.pid == self.pid and dev.vid == self.vid:
                port = dev.device
                logger.info('Found Gauge - {}'.format(port))
        if port is not None:
            try:
                self._ser = serial.Serial(port=port, baudrate=115200, timeout=1)
                return True
            except Exception as e:
                logger.error('Gauge: Failed to open device({}) - {}'.format(port, e))
                self._ser = None

    def read_voltage(self, channel=0):
        """
        Read data from the serial line.
        Example data:
                       CH0:0044\t0.034V
        :return:
        """
        try:
            if self._ser is None:
                self.connect()
            s_time = time.time()
            if self._ser:
                self._ser.reset_input_buffer()
                while time.time() - s_time < 1:
                    data = self._ser.readline().decode().strip()
                    if data.startswith("CH{}:".format(channel)):
                        return float(data.split('\t')[1][:-1])
        except Exception as e:
            logger.error('Failed to read data from gauge - {}'.format(e))
            self._ser = None

    def read_gauge(self):
        volt = self.read_voltage()
        if volt is not None:
            pressure = (volt * 5. - ADC_ZERO_VAL) / ADC_RATIO - PRESSURE_ZERO_VAL
            return round(pressure, 2)

    def close(self):
        if self._ser:
            self._ser.close()
            self._ser = None


if __name__ == '__main__':

    g = GaugeReader()

    while True:
        try:
            print(g.read_gauge())
            time.sleep(1)
        except KeyboardInterrupt:
            break
    g.close()
