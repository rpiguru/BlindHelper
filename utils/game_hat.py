import time

import RPi.GPIO as GPIO

PIN_DATA = dict(
    START=21,
    SELECT=4,
    TL=18,
    TR=23,
    UP=5,
    DOWN=6,
    LEFT=13,
    RIGHT=19,
    A=26,
    B=12,
    X=16,
    Y=20,
)

GPIO.setmode(GPIO.BCM)


def my_callback(channel):
    print('Pin {} is pressed!'.format(channel))


for name, pin in PIN_DATA.items():
    GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(pin, GPIO.FALLING, callback=my_callback)


while True:
    time.sleep(1)
