import threading
import time
import evdev


DEBOUNCE_TIME = .2


class USBButtonReader(threading.Thread):

    def __init__(self, name="DragonRise Inc.   Generic   USB  Joystick  ",
                 codes=(288, 289, 290, 291, 292, 293),
                 callbacks=(None, ) * 6, debug=False, long_press_duration=2):
        super().__init__()
        self.codes = codes
        self.callbacks = callbacks
        self.debug = debug
        self.press_times = {}
        self.last_pressed_times = {}
        self.long_press_duration = long_press_duration
        for code in self.codes:
            self.press_times[code] = 0
            self.last_pressed_times[code] = 0.
        for path in evdev.list_devices():
            dev = evdev.InputDevice(path)
            if dev.name == name:
                print('Found the USB Button Board on `{}`, starting...'.format(path))
                del dev
                self._dev = evdev.InputDevice(path)

    def run(self):
        if not hasattr(self, "_dev"):
            return
        for event in self._dev.read_loop():
            e = evdev.categorize(event)
            if event.type == evdev.ecodes.EV_KEY:
                if e.keystate == e.key_down:
                    if self.debug:
                        print(e)
                    for i, code in enumerate(self.codes):
                        if e.scancode == code:
                            print('Button{} is pressed'.format(i + 1))
                            self.press_times[code] = time.time().__int__()
                elif e.keystate == e.key_up:
                    if self.debug:
                        print(e)
                    for i, code in enumerate(self.codes):
                        if e.scancode == code:
                            if time.time() - self.last_pressed_times[code] > DEBOUNCE_TIME:
                                is_long = (time.time() - self.press_times[code] > self.long_press_duration)
                                print('Button{} is released, LONG PRESS: {}'.format(i + 1, is_long))
                                self.last_pressed_times[code] = time.time()
                                self.callbacks[i](is_long)
                            else:
                                print('Pressed `{}` too quickly!'.format(code))


def print_reading():
    pass


if __name__ == '__main__':

    print('===== Available Devices ====')
    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    for device in devices:
        print(device.path, device.name, device.phys)

    reader = USBButtonReader(callbacks=[print_reading, ] * 6, debug=True)
    reader.start()
