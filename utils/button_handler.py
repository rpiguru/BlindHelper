import threading

import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)


class ButtonHandler(threading.Thread):

    def __init__(self, pin, func, edge='both', bouncetime=200):
        super().__init__(daemon=True)
        self.edge = edge
        self.func = func
        self.pin = pin
        self.bouncetime = float(bouncetime) / 1000.

        self._last_pin_val = GPIO.input(self.pin)
        self._lock = threading.Lock()

    def __call__(self, *args, **kwargs):
        if not self._lock.acquire(blocking=False):
            return
        t = threading.Timer(interval=self.bouncetime, function=self.read, args=args)
        t.start()

    def read(self, *args):
        pin_val = GPIO.input(self.pin)

        if (pin_val == 0 and self._last_pin_val == 1 and self.edge in ['falling', 'both']) or (
                pin_val == 1 and self._last_pin_val == 0 and self.edge in ['rising', 'both']):
            self.func(*args)
        self._last_pin_val = pin_val
        self._lock.release()
