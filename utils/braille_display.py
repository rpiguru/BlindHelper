# -*- coding: utf8 -*-
import usb

from utils.common import logger

"""

- Turn on the Power for the cells

    dev.ctrl_transfer(0x40, 1, 0, 0, msg)

    msg is the 8 byte array again. To turn on the power set the first byte to 0xef else it is 0.

- Display message

    dev.ctrl_transfer(0x40, 10, 0, 0, msg)

    In my case msg is a byte array with 8 bytes. The values for the first 8 cells.

    For the next 8 cells increment the bmRequest from 10 to 11. For the next use 12 and so on.

"""


brailles = [
    '⠀', '⠖', '⠴', '⠼', '⠫', '⠩', '⠯', '⠄', '⠣', '⠜', '⠡', '⠬', '⠂', '⠤', '⠲', '⠌', '⠚',
    '⠁', '⠃', '⠉', '⠙', '⠑', '⠋', '⠛', '⠓', '⠊', '⠒', '⠆', '⠣', '⠿', '⠜', '⠹', '⠈',
    '⠁', '⠃', '⠉', '⠙', '⠑', '⠋', '⠛', '⠓', '⠊', '⠚', '⠅', '⠇', '⠍', '⠝', '⠕', '⠏', '⠟',
    '⠗', '⠎', '⠞', '⠥', '⠧', '⠺', '⠭', '⠽', '⠵', '⠪', '⠳', '⠻', '⠘', '⠸'
]

dot_codes = [
    '', '2-3-5', '3-5-6', '3-4-5-6', '1-2-4-6', '1-4-6', '1-2-3-4-6', '3', '1-2-6', '3-4-5', '1-6', '3-4-6', '2',
    '3-6', '2-5-6', '3-4', '2-4-5',
    '1', '1-2', '1-4', '1-4-5', '1-5', '1-2-4', '1-2-4-5', '1-2-5', '2-4', '2-5', '2-3', '1-2-6', '1-2-3-4-5-6',
    '3-4-5', '1-4-5-6', '4',
    '1', '1-2', '1-4', '1-4-5', '1-5', '1-2-4', '1-2-4-5', '1-2-5', '2-4',
    '2-4-5', '1-3', '1-2-3', '1-3-4', '1-3-4-5', '1-3-5',
    '1-2-3-4', '1-2-3-4-5', '1-2-3-5', '2-3-4', '2-3-4-5', '1-3-6', '1-2-3-6', '2-4-5-6', '1-3-4-6', '1-3-4-5-6',
    '1-3-5-6', '2-4-6', '1-2-5-6', '1-2-4-5-6', '4-5', '4-5-6']

# corresponding ascii codes for Braille symbols
ascii_codes = [
    ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
    'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[', '\\', ']', '^', '_'
]


class BrailleDisplay(object):

    def __init__(self, vid=0x0452, pid=0x0100, cell_count=20):
        self.vid = vid
        self.pid = pid
        self.cell_count = cell_count
        self.dev = None

    def open(self):
        self.dev = usb.core.find(idVendor=self.vid, idProduct=self.pid)
        if self.dev:
            return self.turn_on_device()
        else:
            return False

    def turn_on_device(self):
        if self.dev:
            try:
                self.dev.ctrl_transfer(0x40, 1, 0, 0, [0xef, 0, 0, 0, 0, 0, 0, 0])
                return True
            except Exception as e:
                logger.error('Failed to turn Braille on - {}'.format(e))
                self.dev = None
                return False

    def display_text(self, text):
        if self.dev is None:
            if not self.open():
                return
        try:
            text = text[:self.cell_count]
            txt = str(text).lower() + " " * (self.cell_count - len(text))
            for i in range(len(txt) // 8 + 1):
                values = text2dot(txt[i * 8: i * 8 + 8])
                values.extend([0] * max(0, 8 - len(values)))
                self.dev.ctrl_transfer(0x40, 10 + i, 0, 0, values)
        except Exception as e:
            logger.error('Failed to display text on Braille - {}'.format(e))
            self.dev = None


def text2dot(txt):
    dots = []
    for t in txt:
        if t in ascii_codes:
            str_val = dot_codes[ascii_codes.index(t)]
            if str_val:
                dots.append(sum([2 ** (8 - int(d)) for d in str_val.split('-')]))
            else:
                dots.append(0)
        else:
            dots.append(0)
    return dots


def text2braille(txt):
    result = []
    for t in txt:
        if t in ascii_codes:
            result.append(brailles[ascii_codes.index(t)])
        else:
            result.append(' ')
    return result


if __name__ == '__main__':
    b = BrailleDisplay()
    b.open()
    b.display_text('abcdefghij0123456789')
