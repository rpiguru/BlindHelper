import random

from utils.common import is_rpi

if is_rpi():
    import Adafruit_ADS1x15
    adc = Adafruit_ADS1x15.ADS1115()


# Create an ADS1115 ADC (16-bit) instance.
from settings import VACUUM_GAUGE_CHANNEL, ADC_RATIO, ADC_ZERO_VAL, PRESSURE_ZERO_VAL

# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
GAIN = 2 / 3


def _read_adc(channel=0):
    if is_rpi():
        vals = [adc.read_adc(channel=channel, gain=GAIN) for _ in range(10)]
        vals.remove(max(vals))
        vals.remove(min(vals))
        val = sum(vals) / float(len(vals))
        volt = val * 4.096 / GAIN / 65536 * 2
        return volt
    return random.randint(1, 30) / 10.


def read_gauge():
    volt = _read_adc(channel=VACUUM_GAUGE_CHANNEL)
    pressure = (volt - ADC_ZERO_VAL) / ADC_RATIO - PRESSURE_ZERO_VAL
    return round(pressure, 2)


if __name__ == '__main__':
    import time
    while True:
        try:
            print(_read_adc())
            time.sleep(1)
        except KeyboardInterrupt:
            break
