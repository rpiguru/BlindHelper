import subprocess
import threading

from utils.common import is_rpi, logger, kill_process_by_name

convert_dict = {
    " oz": " ounces",
    " lb": ' pound',
    " lbs": " pounds",
    " pcs": " pieces",
    " kg": " kilograms",
    " g": " grams",
    " in. hg": " inches of mercury",
    "-": "minus "
}


def disconnect_bt_speaker(device):
    threading.Thread(target=_disconnect_bt_speaker, args=(device, )).start()


def _disconnect_bt_speaker(device):
    cmd = "echo -e 'disconnect {}\nquit\n' | bluetoothctl".format(device),
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate()
    logger.info("Disconnected BT speaker, OUT: {}, ERR: {}".format(stdout, stderr))


def trigger_speech(text, device):
    """
        Trigger the current string to speech/braille display
    :param text:
    :type text: str
    :param device: MAC address of the BT speaker
    :return:
    """
    for k, v in convert_dict.items():
        text = text.lower().replace(k, v)
    kill_process_by_name('espeak')
    kill_process_by_name('aplay')
    logger.info('Speaking{} - `{}`'.format("(BT: {})".format(device) if device else "", text))
    if is_rpi() and text.strip():
        if device:
            cmd = "espeak \"{}\" --stdout | aplay -D bluealsa:HCI=hci0,DEV={},PROFILE=a2dp".format(text, device)
        else:
            cmd = "espeak -v english-us \"{}\"".format(text)
        subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
