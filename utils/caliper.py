import os
import threading
import time
import evdev


CODE_MAP_CHAR = {
    'KEY_MINUS': "-",
    'KEY_SPACE': " ",
    'KEY_U': "U",
    'KEY_W': "W",
    'KEY_BACKSLASH': "\\",
    'KEY_GRAVE': "`",
    'KEY_NUMERIC_STAR': "*",
    'KEY_NUMERIC_3': "3",
    'KEY_NUMERIC_2': "2",
    'KEY_NUMERIC_5': "5",
    'KEY_NUMERIC_4': "4",
    'KEY_NUMERIC_7': "7",
    'KEY_NUMERIC_6': "6",
    'KEY_NUMERIC_9': "9",
    'KEY_NUMERIC_8': "8",
    'KEY_NUMERIC_1': "1",
    'KEY_NUMERIC_0': "0",
    'KEY_E': "E",
    'KEY_D': "D",
    'KEY_G': "G",
    'KEY_F': "F",
    'KEY_A': "A",
    'KEY_C': "C",
    'KEY_B': "B",
    'KEY_M': "M",
    'KEY_L': "L",
    'KEY_O': "O",
    'KEY_N': "N",
    'KEY_I': "I",
    'KEY_H': "H",
    'KEY_K': "K",
    'KEY_J': "J",
    'KEY_Q': "Q",
    'KEY_P': "P",
    'KEY_S': "S",
    'KEY_X': "X",
    'KEY_Z': "Z",
    'KEY_KP4': "4",
    'KEY_KP5': "5",
    'KEY_KP6': "6",
    'KEY_KP7': "7",
    'KEY_KP0': "0",
    'KEY_KP1': "1",
    'KEY_KP2': "2",
    'KEY_KP3': "3",
    'KEY_KP8': "8",
    'KEY_KP9': "9",
    'KEY_5': "5",
    'KEY_4': "4",
    'KEY_7': "7",
    'KEY_6': "6",
    'KEY_1': "1",
    'KEY_0': "0",
    'KEY_3': "3",
    'KEY_2': "2",
    'KEY_9': "9",
    'KEY_8': "8",
    'KEY_LEFTBRACE': "[",
    'KEY_RIGHTBRACE': "]",
    'KEY_COMMA': ",",
    'KEY_EQUAL': "=",
    'KEY_SEMICOLON': ";",
    'KEY_APOSTROPHE': "'",
    'KEY_T': "T",
    'KEY_V': "V",
    'KEY_R': "R",
    'KEY_Y': "Y",
    'KEY_TAB': "\t",
    'KEY_DOT': ".",
    'KEY_KPDOT': ".",
    'KEY_SLASH': "/",
    'KEY_ENTER': "\r",
}


"""
NOTE: 
    - Caliper starts with KEY_NUMLOCK and ends with KEY_NUMLOCK
    - The value ends with KEY_ENTER


msg = 'key event at {:f}, {} ({}), {}'
return msg.format(self.event.timestamp(), self.scancode, self.keycode, ks)

key event at 1542657769.467864, 293 (BTN_PINKIE), up

"""


def parse_key_to_char(val):
    return CODE_MAP_CHAR[val] if val in CODE_MAP_CHAR else ""


class CaliperReader(threading.Thread):

    def __init__(self, name="Mitutoyo USB-ITN", callback=None, debug=False):
        super().__init__()
        self.callback = callback
        self.debug = debug
        self.name = name
        self._dev = None
        self.path = None
        self.open()

    def open(self):
        self._dev = None
        for path in evdev.list_devices():
            dev = evdev.InputDevice(path)
            if dev.name == self.name:
                del dev
                self.path = path
                self._dev = evdev.InputDevice(path)

    def get_device(self):
        return self._dev

    def run(self):
        state = 'standby'
        buf = ''
        while True:
            if self._dev is None or not os.path.exists(self.path):
                self.open()
            if self._dev:
                try:
                    event = self._dev.read_one()
                    if event and event.type == evdev.ecodes.EV_KEY:
                        e = evdev.categorize(event)
                        if e.keystate == e.key_up:
                            if self.debug:
                                print(e)
                            if state == 'standby':
                                if e.keycode == 'KEY_NUMLOCK':
                                    state = 'reading'
                                    buf = ''
                            else:
                                if e.keycode == 'KEY_ENTER':
                                    self.callback(buf)
                                elif e.keycode == 'KEY_NUMLOCK':
                                    state = 'standby'
                                else:
                                    buf += parse_key_to_char(e.keycode)
                except Exception as e:
                    print('Failed to read input from {} - {}'.format(self.path, e))
                    self.path = None
                    self._dev = None
                    buf = ''
                    state = 'standby'
            else:
                time.sleep(.1)


def print_reading(val):
    print(val)


if __name__ == '__main__':

    print('===== Available Devices ====')
    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    for device in devices:
        print(device.path, device.name, device.phys)

    reader = CaliperReader(callback=print_reading, debug=True)
    reader.start()
