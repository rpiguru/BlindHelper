from kivy.uix.screenmanager import ScreenManager

from screens.bluetooth_screen import BluetoothScreen
from screens.error_screen import ErrorScreen
from screens.menu_screen import MenuScreen
from screens.repeat_screen import RepeatScreen
from screens.value_screen import ValueScreen


screens = {
    'menu_screen': MenuScreen,
    'value_screen': ValueScreen,
    'repeat_screen': RepeatScreen,
    'bluetooth_screen': BluetoothScreen,
    'error_screen': ErrorScreen,
}


sm = ScreenManager()
