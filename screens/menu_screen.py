import threading

from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from screens.base import ScreenBase
from settings import MEASUREMENTS, SCREEN_TIMEOUT, MEASUREMENT_CONSTANTS
from utils.common import logger, check_git_commit


Builder.load_file('screens/menu_screen.kv')


class MenuScreen(ScreenBase):

    def on_enter(self, *args):
        super(MenuScreen, self).on_enter(*args)
        Clock.schedule_once(lambda dt: self._update_labels())
        self._clk = Clock.schedule_once(lambda dt: self._move_to_value_screen(), SCREEN_TIMEOUT)

    def _update_labels(self):
        for i, m in enumerate(MEASUREMENTS):
            if 'lb_{}'.format(m) in self.ids:
                self.ids['lb_{}'.format(m)].color = (1, 1, 0, 1) if i == self.app.measurement_index else (1, 1, 1, 1)
        self.app.braille.display_text(MEASUREMENT_CONSTANTS.get(self.app.get_cur_measurement()))

    @mainthread
    def on_btn_pressed_5(self):
        self._update_labels()
        if MEASUREMENTS[self.app.measurement_index] != 'update':
            self._cancel_clk()
            self._clk = Clock.schedule_once(lambda dt: self._move_to_value_screen(), SCREEN_TIMEOUT)
        else:
            logger.info('### Checking git update')
            threading.Thread(target=check_git_commit).start()
            self._cancel_clk()

    def _move_to_value_screen(self):
        logger.info('Menu: Switching back to value screen after {} seconds'.format(SCREEN_TIMEOUT))
        self.switch_screen('value_screen')

    def _cancel_clk(self):
        if self._clk:
            self._clk.cancel()
            self._clk = None
