from kivy.clock import Clock
from kivy.lang import Builder

from screens.base import ScreenBase
from settings import SCREEN_TIMEOUT
from utils.common import logger
from utils.espeak import trigger_speech

Builder.load_file('screens/repeat_screen.kv')


speech = {
    3: "Three seconds",
    6: "Six seconds",
    10: "Ten seconds",
    None: "Repeat off",
}


class RepeatScreen(ScreenBase):

    def on_enter(self, *args):
        super(RepeatScreen, self).on_enter(*args)
        Clock.schedule_once(lambda dt: self._update_labels(False))
        self._clk = Clock.schedule_once(lambda dt: self._move_to_value_screen(), SCREEN_TIMEOUT)
        trigger_speech("Select repeat rate", device=self.app.bt_device)

    def _update_labels(self, voice=True):
        for v in {3, 6, 10, None}:
            self.ids['lb_{}'.format(v)].color = (0, 1, 0, 1) if v == self.app.repeat_value else (1, 1, 1, 1)
        self.app.braille.display_text(self.ids['lb_{}'.format(self.app.repeat_value)].text)
        if voice:
            trigger_speech(speech.get(self.app.repeat_value, ""), device=self.app.bt_device)

    def on_btn_pressed_2(self):
        self._update_labels()
        if self._clk:
            self._clk.cancel()
            self._clk = Clock.schedule_once(lambda dt: self._move_to_value_screen(), SCREEN_TIMEOUT)

    def _move_to_value_screen(self):
        logger.info('Repeat: Switching back to value screen after {} seconds'.format(SCREEN_TIMEOUT))
        self.switch_screen('value_screen')
