from kivy.lang import Builder
from screens.base import ScreenBase


Builder.load_file('screens/error_screen.kv')


class ErrorScreen(ScreenBase):

    def on_enter(self, *args):
        error_msg = self.app.get_exception()
        self.ids.error_msg.text = str(error_msg)
        super(ErrorScreen, self).on_enter(*args)
