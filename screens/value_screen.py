from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import DictProperty, StringProperty, BooleanProperty

from screens.base import ScreenBase
from settings import MEASUREMENTS, MEASUREMENT_CONSTANTS
from utils.espeak import trigger_speech


Builder.load_file('screens/value_screen.kv')


class ValueScreen(ScreenBase):

    info = DictProperty()
    cur_reading = StringProperty()
    is_active = BooleanProperty(False)

    def on_enter(self, *args):
        super(ValueScreen, self).on_enter(*args)
        self.info = MEASUREMENT_CONSTANTS.get(MEASUREMENTS[self.app.measurement_index], {})
        self.ids.lb_title.text = self.info.get('title', '')
        self.update_value(self.app.get_current_value())
        self.is_active = True
        if self.app.repeat_value:
            self._clk = Clock.schedule_interval(lambda dt: self._trigger_current_reading(), self.app.repeat_value)

    @mainthread
    def update_value(self, val, speak=True, unit=None):
        self.cur_reading = "{} {}".format(val, self.info.get('unit', '') if unit is None else unit)
        self.display_value(speak=speak)

    def display_value(self, speak=True):
        self.app.braille.display_text(self.cur_reading)
        if speak:
            trigger_speech(self.cur_reading, device=self.app.bt_device)

    def _trigger_current_reading(self):
        if self.is_active:
            if self.app.current_screen.name == self.name and self.cur_reading.strip() and self.app.repeat_value:
                trigger_speech(self.cur_reading, device=self.app.bt_device)
                self.app.braille.display_text(self.cur_reading)
        else:
            if self._clk:
                self._clk.cancel()
                self._clk = None
            return False

    def on_btn_pressed_1(self):
        trigger_speech(self.cur_reading, device=self.app.bt_device)
        self.app.braille.display_text(self.cur_reading)

    def on_pre_leave(self, *args):
        self.is_active = False
        super(ValueScreen, self).on_pre_leave(*args)
