import threading
import time

from kivy.clock import mainthread, Clock
from kivy.lang import Builder
from kivy.properties import ListProperty, NumericProperty, BooleanProperty
from kivy.uix.label import Label

from screens.base import ScreenBase
from utils.bluetooth_ctl import Bluetoothctl
from utils.common import logger
from widgets.snackbar import show_info_snackbar, show_error_snackbar

Builder.load_file('screens/bluetooth_screen.kv')


class BluetoothScreen(ScreenBase):

    devices = ListProperty()
    index = NumericProperty(-1)
    is_connecting = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self.bt = Bluetoothctl()
        self.bt.start_scan()
        self._cur_device_mac = None

    def on_enter(self, *args):
        super(BluetoothScreen, self).on_enter(*args)
        threading.Thread(target=self._tr_scan).start()

    def _tr_scan(self):
        while not self._b_stop.is_set():
            devices = self.bt.get_discoverable_devices()
            # logger.debug("Scanned BT Devices: {}".format(devices))
            if self.app.en_bt and devices:
                Clock.schedule_once(lambda dt: self._add_scanned_devices(devices))
            time.sleep(.1)

    @mainthread
    def _add_scanned_devices(self, devices):
        for dev in devices:
            if dev['mac_address'] not in [d['mac_address'] for d in self.devices]:
                logger.debug("New BT Device: {}".format(dev))
                self.devices.append(dev)
                lb = Label(text="{} ({})".format(dev['mac_address'], dev['name']), size_hint_y=None,
                           height=50, font_size=30, bold=True)
                self.ids.box.add_widget(lb)
                self.ids.box.height += lb.height
        if self.index < 0:
            self.index = 0
            self.ids.box.children[self.index].color = (1, 1, 0, 1)
            self.ids.scr.scroll_to(self.ids.box.children[self.index])

    @mainthread
    def nav_up(self):
        if self.index < 0:
            return
        self.ids.box.children[self.index].color = (1, 1, 1, 1)
        self.index = (self.index + 1) if self.index < (len(self.devices) - 1) else 0
        new_wid = self.ids.box.children[self.index]
        new_wid.color = (1, 1, 0, 1)
        self.ids.scr.scroll_to(new_wid)
        logger.debug('BT: Selected a new device: {}'.format(new_wid.text))
        self._cur_device_mac = new_wid.text.split('(')[0].strip()

    @mainthread
    def nav_down(self):
        if self.index < 0:
            return
        self.ids.box.children[self.index].color = (1, 1, 1, 1)
        self.index = (self.index - 1) if self.index > 0 else (len(self.devices) - 1)
        new_wid = self.ids.box.children[self.index]
        new_wid.color = (1, 1, 0, 1)
        self.ids.scr.scroll_to(new_wid)
        logger.debug('BT: Selected a new device: {}'.format(new_wid.text))
        self._cur_device_mac = new_wid.text.split('(')[0].strip()

    def connect_to_speaker(self):
        if self.index < 0:
            return
        if self.is_connecting:
            logger.warning("Already connecting!")
            return
        logger.info("Connecting to BT Speaker: {}".format(self._cur_device_mac))
        self.is_connecting = True
        threading.Thread(target=self._connect).start()

    def _connect(self):
        val = self.bt.connect(self._cur_device_mac)
        self.is_connecting = False
        Clock.schedule_once(lambda dt: self.on_finished_connect(val, self._cur_device_mac))

    @mainthread
    def on_finished_connect(self, val, mac):
        logger.info("=== BT Connect Result: {}".format(val))
        if val:
            self.app.bt_device = mac
            show_info_snackbar("Connected to BT speaker - {}".format(mac))
        else:
            show_error_snackbar("Failed to connect to BT speaker - {}".format(mac))

    def on_pre_leave(self, *args):
        self._b_stop.set()
        super(BluetoothScreen, self).on_pre_leave(*args)
