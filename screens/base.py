from functools import partial

from kivy.app import App
from kivy.clock import mainthread
from kivy.core.window import Window
from kivy.properties import DictProperty, ObjectProperty
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen

from settings import CONTRAST_MODES
from utils.common import is_rpi


class ScreenBase(Screen):

    app = ObjectProperty()
    color_option = DictProperty(CONTRAST_MODES[0])
    _clk = ObjectProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.app = App.get_running_app()
        self._update_contrast_mode()
        if not is_rpi():
            self.keyboard = Window.request_keyboard(self._keyboard_closed, self)
            self.keyboard.bind(on_key_down=self._on_keyboard_down)

    def switch_screen(self, screen, duration=.3):
        self.app.switch_screen(screen, duration)

    def on_pre_enter(self, *args):
        super(ScreenBase, self).on_pre_enter(*args)
        if not is_rpi():
            for i in range(1, 7):
                btn = Button(text='Button {}'.format(i), size_hint=(None, None), size=(80, 40), pos=(100 * i, 0))
                btn.bind(on_release=partial(self.on_debug_btn_pressed, i))
                self.add_widget(btn)

    def on_pre_leave(self, *args):
        super(ScreenBase, self).on_pre_leave(*args)
        if self._clk:
            self._clk.cancel()
            self._clk = None

    def on_debug_btn_pressed(self, index, *args):
        func = getattr(self.app, 'on_btn_pressed_{}'.format(index))
        func(*args)

    @mainthread
    def update_value(self, val, speak=True, unit=None):
        pass

    def display_value(self, speak=True):
        pass

    def on_btn_pressed_1(self):
        pass

    def on_btn_pressed_2(self):
        pass

    def on_btn_pressed_3(self):
        pass

    def on_btn_pressed_4(self):
        self._update_contrast_mode()

    def on_btn_pressed_5(self):
        pass

    def _update_contrast_mode(self):
        self.color_option = CONTRAST_MODES[self.app.contrast_index]

    def _keyboard_closed(self):
        self.keyboard.unbind(on_key_down=self._on_keyboard_down)
        self.keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'y':
            if self.app.current_screen_name == 'bluetooth_screen':
                self.app.current_screen.connect_to_speaker()
        return True
