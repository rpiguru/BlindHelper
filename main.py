import os
import threading
import traceback
import time

# Import this before Kivy
import conf.config_before
from kivy.app import App
# Import after Kivy
import conf.config_kivy
import widgets.factory_reg

from kivy.clock import Clock, mainthread
from kivy.properties import ObjectProperty, NumericProperty, DictProperty, StringProperty, BooleanProperty
from kivy.uix.screenmanager import FadeTransition, NoTransition
from kivy.base import ExceptionHandler, ExceptionManager
from screens.screen_manager import screens, sm
from settings import INIT_SCREEN, NAME_CALIPER, CONTRAST_MODES, MEASUREMENTS, NAME_USB_BUTTON, BUTTON_CODES, \
    GAME_HAT_PIN_DATA, EN_SCALE, EN_SERIAL, ENABLE_BLUETOOTH
# from utils.adc import read_gauge
from utils.braille_display import BrailleDisplay
from utils.espeak import trigger_speech, disconnect_bt_speaker
from utils.common import is_rpi, get_free_gpu_size, logger, disable_screen_saver
from utils.scale import ScaleReader, HDCSScaleReader
from utils.gauge import GaugeReader
from utils.usb_button import USBButtonReader


if is_rpi():
    import alsaaudio
    import RPi.GPIO as GPIO
    from utils.caliper import CaliperReader
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)


class BHExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        logger.exception(exception)
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(BHExceptionHandler())


class BlindHelperApp(App):

    current_screen = ObjectProperty(None, allownone=True)
    current_screen_name = StringProperty()
    exception = None

    lock = threading.RLock()
    contrast_index = NumericProperty(0)
    repeat_value = ObjectProperty(None, allownone=True)
    measurement_index = NumericProperty(0)
    data = DictProperty({
        'caliper': '',
        'pressure': 0,
        'counter': 0,
        'scale': 0,
        'serial': 0,
        'update': None
    })
    braille = None

    en_bt = BooleanProperty(ENABLE_BLUETOOTH)
    bt_device = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if EN_SCALE:
            threading.Thread(target=self._read_scale, ).start()
        if EN_SERIAL:
            threading.Thread(target=self._read_hdcs_scale, ).start()

        if is_rpi():

            self.audio = alsaaudio.Mixer('PCM')

            self._caliper = CaliperReader(name=NAME_CALIPER, callback=self.on_caliper_reading)
            if self._caliper.get_device():
                logger.info('Found a caliper({}), starting...'.format(self._caliper.get_device()))
                self._caliper.start()

            # GAME HAT Buttons
            for name, pin in GAME_HAT_PIN_DATA.items():
                GPIO.setup(pin, GPIO.IN, GPIO.PUD_UP)
                GPIO.add_event_detect(pin, GPIO.FALLING, callback=self.on_game_hat_button_pressed, bouncetime=200)

        # Start USB Button Reader
        self._btn_reader = \
            USBButtonReader(name=NAME_USB_BUTTON, codes=BUTTON_CODES,
                            callbacks=[getattr(self, 'on_btn_pressed_{}'.format(i)) for i in range(1, 7)])
        self._btn_reader.start()

        self.gauge = GaugeReader()

        self.braille = BrailleDisplay()
        if not self.braille.open():
            logger.error('Not Found Braille display!')

    def build(self):
        threading.Thread(target=self._read_gauge, ).start()
        self.switch_screen(INIT_SCREEN)
        return sm

    @mainthread
    def switch_screen(self, screen_name, duration=0):
        s_time = time.time()
        if sm.has_screen(screen_name):
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            sm.transition = FadeTransition(duration=duration) if duration else NoTransition()
            sm.switch_to(screen)
            msg = ' :: GPU - {}'.format(get_free_gpu_size()) if is_rpi() else ''
            logger.info('BH: ===== Switched to {} screen {}, Elapsed: {} ====='.format(
                screen_name, msg, time.time() - s_time))
            if self.current_screen:
                sm.remove_widget(self.current_screen)
            self.current_screen = screen
        self.current_screen_name = screen_name

    def _read_scale(self):
        _scale = ScaleReader()
        while True:
            data = _scale.read_data()
            if data:
                logger.info('Scale: {}'.format(data))
                Clock.schedule_once(lambda dt: self.update_reading_from_sensor('scale', data, True))
            time.sleep(.5)

    def _read_hdcs_scale(self):
        hdcs_scale = HDCSScaleReader()
        while True:
            data = hdcs_scale.read_data()
            if data:
                logger.info('HDCS Scale: {}'.format(data))
                Clock.schedule_once(
                    lambda dt: self.update_reading_from_sensor('serial', data['value'], True, data['unit']))
            time.sleep(.5)

    def _read_gauge(self):
        s_time = 0
        while True:
            pressure = self.gauge.read_gauge()
            if pressure is not None:
                if time.time() - s_time > 10:
                    logger.info('Pressure: {} in. Hg'.format(pressure))
                    s_time = time.time()
                Clock.schedule_once(lambda dt: self.update_reading_from_sensor('pressure', pressure, False))
            time.sleep(.5)

    @mainthread
    def update_reading_from_sensor(self, dev, value, speak=False, unit=None):
        with self.lock:
            if self.get_cur_measurement() == dev:
                self.data[dev] = value
                self.current_screen.update_value(value, speak, unit)

    def on_caliper_reading(self, val):
        try:
            caliper_val = float(val)
            logger.info('Caliper : {}'.format(caliper_val))
            with self.lock:
                if self.get_cur_measurement() == 'caliper':
                    self.data['caliper'] = caliper_val
                    self.current_screen.update_value(caliper_val, speak=self.repeat_value is None)
        except ValueError:
            logger.error('Invalid caliper reading - `{}`'.format(val))

    @mainthread
    def on_btn_pressed_1(self, *args):
        logger.info('Button1 is pressed - {}'.format(args))
        with self.lock:
            if self.get_cur_measurement() == 'counter':
                self.data['counter'] += 1
                self.current_screen.update_value(self.data['counter'])
            else:
                self.current_screen.on_btn_pressed_1()
            logger.info('Current counter: {}'.format(self.data['counter']))

    @mainthread
    def on_btn_pressed_2(self, *args):
        logger.info('Button2 is pressed - {}'.format(args))
        is_long = args[0]
        with self.lock:
            cur_measurement = self.get_cur_measurement()
            if self.current_screen_name == 'repeat_screen' or is_long:
                self._toggle_repeat_mode()
            elif cur_measurement == 'counter':
                self.data['counter'] -= 1
                self.current_screen.update_value(self.data['counter'])
            elif cur_measurement == 'caliper':
                self.data['caliper'] = ''
                self.current_screen.update_value(self.data['caliper'])
            self.current_screen.on_btn_pressed_2()
            logger.info('Current counter: {}'.format(self.data['counter']))

    @mainthread
    def on_btn_pressed_3(self, *args):
        logger.info('Button3 is pressed - {}'.format(args))
        with self.lock:
            if self.get_cur_measurement() == 'counter':
                self.data['counter'] = 0
                self.current_screen.update_value(self.data['counter'])
            logger.info('Current counter: {}'.format(self.data['counter']))

    @mainthread
    def on_btn_pressed_4(self, *args):
        logger.info('Button4 is pressed - {}'.format(args))
        with self.lock:
            self._update_contrast_mode(mode='increase')

    @mainthread
    def on_btn_pressed_5(self, *args):
        logger.info('Button5 is pressed - {}'.format(args))
        with self.lock:
            self._toggle_measurement_mode()

    def on_btn_pressed_6(self, *args):
        logger.info('Button6 is pressed - {}'.format(args))
        logger.warning('!!! Button6 is pressed! Rebooting now...')
        if is_rpi():
            os.system('sudo reboot')
        else:
            self.stop()

    def get_cur_measurement(self):
        return MEASUREMENTS[self.measurement_index]

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception

    def on_game_hat_button_pressed(self, channel):
        Clock.schedule_once(lambda dt: self.parse_game_hat_button(channel))

    @mainthread
    def parse_game_hat_button(self, channel):
        btn_name = None
        for name, pin in GAME_HAT_PIN_DATA.items():
            if pin == channel:
                btn_name = name
                break
        logger.info('GAME HAT: `{}` button is pressed!'.format(btn_name))
        is_bt_screen = self.current_screen_name == 'bluetooth_screen'
        with self.lock:
            if btn_name == 'UP':                                            # Volume Up
                if is_bt_screen:
                    Clock.schedule_once(lambda dt: self.current_screen.nav_up())
                elif is_rpi():
                    cur_vol = int(self.audio.getvolume()[0])
                    self.audio.setvolume(min(cur_vol + 4, 100))
                    logger.info('New Volume: {}'.format(self.audio.getvolume()[0]))
            elif btn_name == 'DOWN':                                        # Volume Down
                if is_bt_screen:
                    Clock.schedule_once(lambda dt: self.current_screen.nav_down())
                elif is_rpi():
                    cur_vol = int(self.audio.getvolume()[0])
                    self.audio.setvolume(max(cur_vol - 4, 60))
                    logger.info('New Volume: {}'.format(self.audio.getvolume()[0]))
            elif btn_name == 'LEFT':                                        # Decrease color mode index
                self._update_contrast_mode(mode='decrease')
            elif btn_name == 'RIGHT':                                       # Increase color mode index
                self._update_contrast_mode(mode='increase')
            elif btn_name == 'START':                                       # Hold to toggle Repeat Selection
                if not is_bt_screen:
                    self._toggle_repeat_mode()
            elif btn_name == 'SELECT':                                      # Reset Count
                if self.get_cur_measurement() == 'counter':
                    self.data['counter'] = 0
                    self.current_screen.update_value(self.data['counter'])
                    logger.info('Current counter: {}'.format(self.data['counter']))
            elif btn_name == 'X':                                           # Increase Count
                if self.get_cur_measurement() == 'counter':
                    self.data['counter'] += 1
                    self.current_screen.update_value(self.data['counter'])
                    logger.info('Current counter: {}'.format(self.data['counter']))
            elif btn_name == 'A':                                           # Speak current info
                if is_bt_screen:
                    self.en_bt = not self.en_bt
                    if not self.en_bt:
                        logger.info("Disabling bluetooth now...")
                        if self.bt_device:
                            disconnect_bt_speaker(self.bt_device)
                        self.bt_device = ""
                    else:
                        logger.info("Bluetooth is enabled")
                else:
                    self.current_screen.display_value(speak=True)
            elif btn_name == 'B':                                           # Decrease Count
                if self.get_cur_measurement() == 'counter':
                    self.data['counter'] -= 1
                    self.current_screen.update_value(self.data['counter'])
                    logger.info('Current counter: {}'.format(self.data['counter']))
            elif btn_name == 'TR':                                          # Toggle cycle through main functions
                self._toggle_measurement_mode()
            elif btn_name == 'TL':                                          # press & hold for 10 sec to reboot
                threading.Thread(target=self.reboot_timer, ).start()
            elif btn_name == 'Y':
                if is_bt_screen:
                    self.current_screen.connect_to_speaker()

    def _update_contrast_mode(self, mode='increase'):
        if mode == 'increase':
            if self.contrast_index < len(CONTRAST_MODES) - 1:
                self.contrast_index += 1
            else:
                self.contrast_index = 0
        else:
            if self.contrast_index == 0:
                self.contrast_index = len(CONTRAST_MODES) - 1
            else:
                self.contrast_index -= 1
        logger.info('Switched contrast option to {}'.format(self.contrast_index))
        self.current_screen.on_btn_pressed_4()

    def _toggle_repeat_mode(self):
        repeats = [3, 6, 10, None]
        index = repeats.index(self.repeat_value)
        if index < len(repeats) - 1:
            self.repeat_value = repeats[index + 1]
        else:
            self.repeat_value = repeats[0]
        logger.info('Current repeat rate: {}'.format(self.repeat_value))
        if self.current_screen_name == 'repeat_screen':
            self.current_screen.on_btn_pressed_2()
        else:
            self.switch_screen('repeat_screen')

    def _toggle_measurement_mode(self):
        if self.measurement_index < len(MEASUREMENTS) - 1:
            self.measurement_index += 1
        else:
            self.measurement_index = 0
        if self.get_cur_measurement() != 'bluetooth':
            self.switch_screen('menu_screen')
            Clock.schedule_once(lambda dt: self.current_screen.on_btn_pressed_5())
        else:
            self.en_bt = True
            self.switch_screen('bluetooth_screen')
        logger.info('Current measurement: {}'.format(self.get_cur_measurement()))
        trigger_speech(
            text="{} mode is selected".format(self.get_cur_measurement().replace('pressure', 'vacuum')),
            device=self.bt_device
        )

    def get_current_value(self):
        return self.data.get(self.get_cur_measurement(), '')

    def reboot_timer(self):
        s_time = time.time()
        while not GPIO.input(GAME_HAT_PIN_DATA['TL']):
            if time.time() - s_time > 10:
                logger.warning('=== You pressed TL button for 10 sec! Rebooting...')
                if is_rpi():
                    os.system('sudo reboot')
                else:
                    self.stop()
            else:
                time.sleep(.1)


if __name__ == '__main__':

    logger.info('========== Starting Blind Helper ==========')

    disable_screen_saver()

    app = BlindHelperApp()
    app.run()
