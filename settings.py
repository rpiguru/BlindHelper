

# Switch to the value screen after 2 sec
SCREEN_TIMEOUT = 2

VACUUM_GAUGE_CHANNEL = 0

NAME_CALIPER = "Mitutoyo USB-ITN"
NAME_USB_BUTTON = "DragonRise Inc.   Generic   USB  Joystick  "

GAUGE_VID = 0x1A86
GAUGE_PID = 0x7523

SCALE_VID = 0x0403
SCALE_PID = 0x6015

HDCS_SCALE_VID = 0x0403
HDCS_SCALE_PID = 0x6001


READING_INTERVAL = .5

INIT_SCREEN = 'menu_screen'


# Constant Values for the Vacuum Gauge
ADC_ZERO_VAL = 0.15
ADC_RATIO = 0.14
PRESSURE_ZERO_VAL = 0

ENABLE_BLUETOOTH = False

CONTRAST_MODES = [
    {
        'background': (0, 0, 0, 1),
        'foreground': (1, 1, 1, 1),
    },
    {
        'background': (0, 0, 0, 1),
        'foreground': (1, 1, 0, 1),
    },
    {
        'background': (0, 0, 1, 1),
        'foreground': (1, 1, 0, 1),
    },
    {
        'background': (1, 1, 1, 1),
        'foreground': (0, 0, 0, 1),
    },
    {
        'background': (255 / 255., 161 / 255., 0 / 255., 1),
        'foreground': (0, 0, 0, 1),
    },
    {
        'background': (0, 0, 1, 1),
        'foreground': (1, 1, 1, 1),
    },
    {
        'background': (1, 1, 0, 1),
        'foreground': (0, 0, 1, 1),
    },
]

MEASUREMENTS = ['caliper', 'pressure', 'counter', 'scale', 'serial', 'update', 'bluetooth']

MEASUREMENT_CONSTANTS = dict(
    scale=dict(
        title='SCALE',
        unit=''
    ),
    counter=dict(
        title='COUNTER',
        unit='',
    ),
    pressure=dict(
        title='VACUUM',
        unit='in. Hg',
    ),
    caliper=dict(
        title='MEASURE',
        unit='',
    ),
    serial=dict(
        title='SERIAL',
        unit=''
    ),
    update=dict(
        title='',
        unit=''
    ),
    bluetooth=dict(
        title='BLUETOOTH',
        unit=""
    )
)


"""
Button 1: 288 (['BTN_JOYSTICK', 'BTN_TRIGGER']), up
Button 2: 289 (BTN_THUMB), up 
Button 3: 290 (BTN_THUMB2), up
Button 4: 291 (BTN_TOP), up
Button 5: 292 (BTN_TOP2), up
Button 6: 293 (BTN_PINKIE), up
"""
BUTTON_CODES = [288, 289, 290, 291, 292, 293]

# Button type. USB/HAT
# BUTTON_TYPE = 'USB'
BUTTON_TYPE = 'HAT'

EN_SCALE = True

EN_SERIAL = True

GAME_HAT_PIN_DATA = dict(
    START=21,
    SELECT=4,
    TL=23,
    TR=18,
    UP=5,
    DOWN=6,
    LEFT=13,
    RIGHT=19,
    A=26,
    B=12,
    X=16,
    Y=20,
)


try:
    from local_settings import *
except ImportError:
    pass
